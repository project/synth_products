
synth_products module for Drupal 5.x:

A module to track chemicals, compounds, or other reagents (generically speaking,
"products") that are synthesized in a laboratory and then which may be used or
sent to collaborating labs. Each use can be recorded as a transaction.

Products and transactions are each defined as new content types, and with a 
relationship linking a single product and many transactions.

While the code could be generally adapted for inventory management, the 
terminology here is somewhat chemistry specific.

by peter wolanin (pwolanin@drupal).